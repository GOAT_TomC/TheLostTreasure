﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dynamite : PoolObject {

    private ObjectPool poolFromObject;
    private ObjectPool m_SFXPool;

    private Animator animator;

    [SerializeField] private float impactRange;
    [SerializeField] private LayerMask layermask;

    public override void Initialize(ObjectPool objectPool)
    {
        poolFromObject = objectPool;
        m_SFXPool = GameObject.Find("BombSFXPool").GetComponent<ObjectPool>();
        animator = this.GetComponent<Animator>();
    }

    public override void Enable()
    {
        this.gameObject.SetActive(true);
    }

    public override void Disable()
    {
        poolFromObject.AddToPool(this);
        this.gameObject.SetActive(false);
    }

    public void Explode()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(this.transform.position, impactRange, layermask);
       
        foreach (Collider2D col in hitColliders)
        {
            if (col.CompareTag("Destructable"))
            {
                col.gameObject.SetActive(false);
            }
        }
    }

    public void ShakeCam()
    {
        CameraShake.cameraShake.ShakeCamera(0.1f, 1f);
    }

    private void PlaySFX()
    {
        PoolObject SFX = m_SFXPool.GetAvailableObjectInPool();
        SFX.transform.position = this.transform.position;
        SFX.Enable();
    }
}

public abstract class PoolObject : MonoBehaviour
{
    public abstract void Initialize(ObjectPool objectPool);
    public abstract void Enable();
    public abstract void Disable();
}
