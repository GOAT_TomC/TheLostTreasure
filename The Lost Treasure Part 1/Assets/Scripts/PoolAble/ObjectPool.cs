﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    [SerializeField] private PoolObject objectToPool;
    [SerializeField] private int poolAmount;

    private List<PoolObject> availableInPool;

    // Use this for initialization
    void Start()
    {
        InitiliazePool();
    }

    private void InitiliazePool()
    {
        availableInPool = new List<PoolObject>();

        PoolObject tempObject;

        for (int i = 0; i < poolAmount; ++i)//Initialize the pool decided by the object to pool and poolAmount
        {
            tempObject = Instantiate(objectToPool, this.transform.position, Quaternion.identity);
            tempObject.transform.parent = this.transform;
            availableInPool.Add(tempObject);
            tempObject.Initialize(this);
            tempObject.Disable();
        }

    }

    public PoolObject GetAvailableObjectInPool()
    {
        PoolObject objectToReturn;

        if (availableInPool.Count > 0) //Check if there are objects available
        {
            objectToReturn = availableInPool[availableInPool.Count - 1];
            availableInPool.RemoveAt(availableInPool.Count - 1);
            return objectToReturn;
        }

        //We get here if there is no available object
        //Use/Return emergency function because we don't want the player to notice

        return GrowPool();
    }

    private PoolObject GrowPool() //Emergeny function in case of a to little object pool
    {
        PoolObject tempObject = Instantiate(objectToPool, this.transform.position, Quaternion.identity);
        tempObject.transform.parent = this.transform;
        tempObject.Initialize(this);
        tempObject.Disable();

        return tempObject;
    }

    public void AddToPool(PoolObject poolObject) //Function the pool object calls when it can be used again
    {
        availableInPool.Add(poolObject);
    }

}
