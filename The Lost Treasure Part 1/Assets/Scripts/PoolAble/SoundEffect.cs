﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : PoolObject {

    [SerializeField]
    private AudioSource m_SFX;

    private ObjectPool m_ObjectPool;
    private float m_LifeTime;

    public override void Initialize(ObjectPool objectPool)
    {
        m_LifeTime = m_SFX.clip.length;
        m_SFX.volume = GameManager.Instance.SFXVolume;
        m_ObjectPool = objectPool;
    }

    public override void Enable()
    {
        this.gameObject.SetActive(true);
        m_SFX.Play();
        Invoke("Disable", m_LifeTime);
    }

    public override void Disable()
    {
        m_ObjectPool.AddToPool(this);
        this.gameObject.SetActive(false);
    }
}
