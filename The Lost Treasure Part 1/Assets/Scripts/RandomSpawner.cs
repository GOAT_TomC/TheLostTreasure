﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : Spawner {

    protected override void SpawnObject()
    {
        PoolObject objectToSpawn = m_PoolToSpawnFrom.GetAvailableObjectInPool(); // TODO Figure out why it works after checking

        if (objectToSpawn.gameObject.activeInHierarchy)
        {
            //Debug.LogError("Object allready active");
            objectToSpawn = m_PoolToSpawnFrom.GetAvailableObjectInPool();
        }

        objectToSpawn.transform.position = new Vector3(m_SpawnPosition.position.x + Random.Range(-1, 1), m_SpawnPosition.position.y + Random.Range(-1, 1));
        objectToSpawn.Enable();
        ++m_SpawnedObjects;

        if (m_SpawnedObjects <= m_SpawnAmount)
        {
            Invoke("SpawnObject", m_SpawnInterval);
        }
    }
}
