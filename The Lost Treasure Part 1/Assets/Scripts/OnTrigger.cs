﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class OnTrigger : MonoBehaviour {

    private bool m_First = true;

    [SerializeField] private List<string> m_TriggerNames;
    [SerializeField] private List<Animator> m_Animators;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && m_First)
        {
            m_First = false;
            for (int i = 0; i < m_TriggerNames.Count; ++i)
            {
                m_Animators[i].gameObject.SetActive(true);
                m_Animators[i].SetBool(m_TriggerNames[i], true);
            }
        }
    }
}
