﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreyOut : MonoBehaviour {

    private const int DIVIDE_FACTOR = 115;

    private Image m_Image;
    private Color m_Color;
    [SerializeField]
    private float m_Alpha = 0;

	// Use this for initialization
	void Start () {
        GameObject.Find("Player").GetComponent<PlayerController>().OnDamage += LowerTransparency;
        GameObject.Find("Player").GetComponent<PlayerController>().OnHealthRegen += AddTransparency;
        GameObject.Find("Player").GetComponent<PlayerController>().OnReset += ResetValues;
        m_Image = this.GetComponent<Image>();
        m_Color = Color.grey;
        m_Color.a = m_Alpha;
        m_Image.color = m_Color;
    }
	
	// Update is called once per frame
	void Update () {
	}

    public void LowerTransparency(float i)
    {
        m_Alpha += i / DIVIDE_FACTOR;
        m_Color.a = m_Alpha;
        m_Image.color = m_Color;
    }

    public void AddTransparency(float i)
    {
        m_Alpha -= i / DIVIDE_FACTOR;
        m_Color.a = m_Alpha;
        m_Image.color = m_Color;
    }

    public void ResetValues()
    {
        m_Alpha = 0;
        m_Color.a = m_Alpha;
        m_Image.color = m_Color;
    }
}
