﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {

    [SerializeField] private Dialogue m_Dialogue;

	// Use this for initialization
	void Start () {

            Invoke("StartDialogue", 1f); //Make sure everything is started
	}

    private void StartDialogue()
    {
        GameManager.Instance.DialogueManager.StartDialogue(m_Dialogue, null);
    }
}
