﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour {

    private const float FADE_SPEED = 0.5f;

    [SerializeField] Image m_FadeImage;
    private Color m_Color;

	// Use this for initialization
	void Start () {
        m_Color = Color.clear;
        m_FadeImage.color = m_Color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    private void EndScene()
    {
        FadeToBlack();
    }

    private void FadeToBlack()
    {
        m_FadeImage.color = Color.Lerp(m_FadeImage.color, Color.black, FADE_SPEED * Time.deltaTime);
        if (m_FadeImage.color.a <= 0.95f)
        {
            Invoke("FadeToBlack", Time.deltaTime);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            m_FadeImage.gameObject.SetActive(true);
            EndScene();
        }
    }
}
