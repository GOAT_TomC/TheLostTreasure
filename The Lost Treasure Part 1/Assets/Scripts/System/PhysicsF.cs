﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsF : MonoBehaviour {

    //AddsForceToAnObject for a certain amount of time
    public void AddForceToObject(Transform target, Vector3 forceToAdd)
    {
        StartCoroutine(AddForceToObjectRoutine(target, forceToAdd));
    }

    private IEnumerator AddForceToObjectRoutine(Transform target, Vector3 forceToAdd)
    {

        float time = 0.2f;

        float elapsedTime = 0;

        Vector3 startingPos = target.position;

        while (elapsedTime < time)
        {
            target.position = Vector3.Lerp(target.position, target.position + forceToAdd, (elapsedTime / time));
            elapsedTime += Time.deltaTime;

            yield return null;
        }
    } // TODO Learn Friction now its fake with time
}
