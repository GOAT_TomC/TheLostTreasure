﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {


    [SerializeField] private AudioSource m_MainBackgroundMusic;
    [SerializeField] private AudioSource m_FightBackgroundMusic;
    [SerializeField] private  float m_FadeSpeedFight;
    [SerializeField] private float m_FadeSpeedMain;

    private bool m_Fade = true; //True for fade in Attack / false for fade main

    private Coroutine m_RunningCoroutine;

    // Amount of enemies that are currently attacking
    private int m_AttackingAmount;

    public void AddAttacking()
    {
        ++m_AttackingAmount;
        SwitchMusic();
    }

    public void RemoveAttacking()
    {
        if (m_AttackingAmount != 0)
            --m_AttackingAmount;
        SwitchMusic();
    }

    private void SwitchMusic()
    {
        if (m_AttackingAmount > 0)
        {
            if (m_Fade)
            {
                if (m_RunningCoroutine != null)
                    StopCoroutine(m_RunningCoroutine);

                m_RunningCoroutine = StartCoroutine(FadeMusic(m_FightBackgroundMusic, m_MainBackgroundMusic, m_FadeSpeedFight));
                m_Fade = false;
            }
        }
        else
        {
            if (!m_Fade)
            {
                if (m_RunningCoroutine != null)
                    StopCoroutine(m_RunningCoroutine);

                m_RunningCoroutine = StartCoroutine(FadeMusic(m_MainBackgroundMusic, m_FightBackgroundMusic, m_FadeSpeedMain));
                m_Fade = true;
            }
        }
    }

    public IEnumerator FadeMusic(AudioSource newAudio, AudioSource oldAudio, float fadeSpeed)
    {
        newAudio.volume = 0.000f;
        newAudio.Play();
        while (newAudio.volume < 1f)
        {
            oldAudio.volume -= fadeSpeed * Time.deltaTime;
            newAudio.volume += fadeSpeed * Time.deltaTime;

            yield return new WaitForSeconds(0.001f);
        }
        oldAudio.volume = 0.000f; // Just In Case....
        oldAudio.Stop();
    }
}
