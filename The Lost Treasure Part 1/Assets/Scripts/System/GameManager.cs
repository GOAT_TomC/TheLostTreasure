﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager m_Instance;
    public static GameManager Instance { get { return m_Instance; } }

    private MusicPlayer m_MusicPlayer;
    public MusicPlayer MusicPlayer { get { return m_MusicPlayer; } }

    private DialogueManager m_DialogueManager;
    public DialogueManager DialogueManager { get { return m_DialogueManager; } }

    private PhysicsF m_Physics;
    public PhysicsF Physics { get { return m_Physics; } }

    private float m_SFXVolume;
    public float SFXVolume { get { return m_SFXVolume; } }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Debug.LogError("More than one instance of 'GameManager'");
        }

        m_MusicPlayer = this.GetComponent<MusicPlayer>();
        m_Physics = this.GetComponent<PhysicsF>();
        m_DialogueManager = this.GetComponent<DialogueManager>();
        SetDefaultValues();
    }

    private void SetDefaultValues()
    {
        m_SFXVolume = 1;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
