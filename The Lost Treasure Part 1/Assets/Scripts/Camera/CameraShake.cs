﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    public static CameraShake cameraShake;

    private float shakeTimer;
    private float shakeAmount;

    private void Awake()
    {
        cameraShake = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (shakeTimer >= 0)
        {
            Vector2 shakePosition = Random.insideUnitCircle * shakeAmount;

            this.transform.position = new Vector3(this.transform.position.x + shakePosition.x, this.transform.position.y + shakePosition.y, this.transform.position.z);

            shakeTimer -= Time.deltaTime;
        }
	}

    public void ShakeCamera(float shakePower, float shakeDuration)
    {
        shakeAmount = shakePower;
        shakeTimer = shakeDuration;
    }
}
