﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    [SerializeField]private float dampTime;
    private Vector3 velocity = Vector3.zero;
    private Transform target;
    private Camera cam;

	// Use this for initialization
	void Start () {
        target = GameObject.Find("Player").transform;
        cam = this.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        FollowPlayer();
	}

    private void FollowPlayer()
    {
        if (target)
        {
            Vector3 point = cam.WorldToViewportPoint(target.position); //Get point in world
            Vector3 delta = target.position - cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //Get the diffrence of the target and current position
            Vector3 destination = transform.position + delta; // Destination when delta added
            transform.position = Vector3.SmoothDamp(this.transform.position, destination, ref velocity, dampTime); //move to the destination
        }
    }
}
