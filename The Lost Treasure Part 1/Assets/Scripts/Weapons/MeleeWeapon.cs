﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : MonoBehaviour {

    [SerializeField]protected float damage;
    [SerializeField] protected float DamageRange;
    [SerializeField] protected LayerMask layerMask;

    private ObjectPool m_HitSFXPool;
    private ObjectPool m_SwingSFXPool;

    protected float coolDown;
    private bool canAttack = true;

    protected Animator weaponAnimator;
    protected Transform parentTransform;

	// Use this for initialization
	protected virtual void Start () {
        weaponAnimator = this.GetComponent<Animator>();
        parentTransform = this.transform.parent.transform;
        m_HitSFXPool = GameObject.Find("HitSFXPool").GetComponent<ObjectPool>();
        m_SwingSFXPool = GameObject.Find("SwingSFXPool").GetComponent<ObjectPool>();
        GetAnimLength();
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        //InputHandler();
	}

    protected virtual void InputHandler()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DoAttack();
        }
    }

    public virtual void DoAttack()
    {
        if (canAttack)
        {
            weaponAnimator.SetTrigger("Attack");
            canAttack = false;
            Invoke("AllowNewAttack", coolDown);
            PlaySwingSFX();
        }
    }

    protected virtual void AllowNewAttack()
    {
        canAttack = true;
    }

    protected virtual void GetAnimLength()
    {
        AnimationClip[] clips = weaponAnimator.runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            if (clip.name == "AttackUp")
            {
                coolDown = clip.length;
                //Debug.Log(clip.length);
                return;
            }
        }
    }

    public virtual void UpdatePosition(Direction playerDir)
    {
        switch (playerDir)
        {
            case Direction.north:
                this.transform.position = new Vector2(0.235f + parentTransform.position.x, -0.166f + parentTransform.position.y);
                //this.transform.localEulerAngles = new Vector3(-0.467f, 0.234f, -299.7f);
                break;
            case Direction.east:
                this.transform.position = new Vector2(-0.037f + parentTransform.position.x, -0.228f + parentTransform.position.y);
                //this.transform.localEulerAngles = new Vector3(-0.03f, -0.459f, -55.119f);
                break;
            case Direction.south:
                this.transform.position = new Vector2(-0.208f + parentTransform.position.x, -0.166f + parentTransform.position.y);
                //this.transform.localEulerAngles = new Vector3(-0.467f, 0.234f, -131.507f);
                break;
            case Direction.west:
                //this.transform.localEulerAngles = new Vector3(0f, 180f, -36.86f);
                this.transform.position = new Vector2(0.056f + parentTransform.position.x, 0.056f + parentTransform.position.y);
                break;

        }

        UpdateAnimator(playerDir);

    }

    private void UpdateAnimator(Direction playerDir)
    {
        weaponAnimator.SetFloat("Dir", (float)playerDir);
    }

    protected virtual void GiveDamage()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(this.transform.position, DamageRange, layerMask);

        foreach(Collider2D coll in colliders)
        {
            coll.SendMessage("GetDamage", damage);
            PlayHitSoundFX();
        }
    }

    private void PlayHitSoundFX()
    {
        PoolObject HitSFX = m_HitSFXPool.GetAvailableObjectInPool();
        HitSFX.transform.position = this.transform.position;
        HitSFX.Enable();
    }

    private void PlaySwingSFX()
    {
        PoolObject SFX = m_SwingSFXPool.GetAvailableObjectInPool();
        SFX.transform.position = this.transform.position;
        SFX.Enable();
    }
}
