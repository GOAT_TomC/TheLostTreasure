﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField] protected ObjectPool m_PoolToSpawnFrom;
    [SerializeField] protected float m_SpawnInterval;
    [SerializeField] protected Transform m_SpawnPosition;
    [SerializeField] protected int m_SpawnAmount;
    protected int m_SpawnedObjects;

    private bool m_AllowSpawnActivation = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivateSpawner()
    {
        if (m_AllowSpawnActivation)
        {
            Invoke("SpawnObject", m_SpawnInterval);
        }
    }

    protected virtual void SpawnObject()
    {
        PoolObject objectToSpawn = m_PoolToSpawnFrom.GetAvailableObjectInPool(); // TODO Figure out why it works after checking

        if (objectToSpawn.gameObject.activeInHierarchy)
        {
            //Debug.LogError("Object allready active");
            objectToSpawn = m_PoolToSpawnFrom.GetAvailableObjectInPool();
        }

        objectToSpawn.transform.position = m_SpawnPosition.position;
        objectToSpawn.Enable();
        ++m_SpawnedObjects;

        if (m_SpawnedObjects <= m_SpawnAmount)
        {
            Invoke("SpawnObject", m_SpawnInterval);
        }
    }
}
