﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuHandler : MonoBehaviour {

    private GameObject m_Menu;
    private GameObject m_BombIndicator;

    private void Awake()
    {
        m_Menu = GameObject.Find("GameMenu");
        m_BombIndicator = GameObject.Find("BombIndicator");
        GameObject.Find("Player").GetComponent<PlayerController>().InventoryChange += ToggleBombDisplay;
    }

    // Use this for initialization
    void Start () {
        m_BombIndicator.SetActive(false);
        m_Menu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        InputHandle();
	}

    private void InputHandle()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            DisplayToggle();
        }
    }

    public void SwitchScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void DisplayToggle()
    {
        m_Menu.SetActive(!m_Menu.activeInHierarchy);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    private void ToggleBombDisplay(bool display)
    {
        m_BombIndicator.SetActive(display);
    }
}
