﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivate : MonoBehaviour {

    [SerializeField] private GameObject m_ObjectToDisable;

    private ObjectPool m_ObjectPool;

    private void Start()
    {
        m_ObjectPool = GameObject.Find("BombSFXPool").GetComponent<ObjectPool>();
    }

    public void Disable()
    {
        m_ObjectToDisable.SetActive(false);
    }

    public void PlaySound()
    {
        PoolObject sfx = m_ObjectPool.GetAvailableObjectInPool();
        sfx.transform.position = this.transform.position;
        sfx.Enable();
    }
}
