﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardAttack : AttackBehaviour
{

    private MeleeWeapon m_MeleeWeapon;
    private Transform m_Target;
    [SerializeField] private float m_MinDistance;

    public override void Initialize(MeleeWeapon weapon, Transform target)
    {
        m_MeleeWeapon = weapon;
        m_Target = target;
    }

    public override void CheckForOppertunity()
    {
        if (Vector3.Distance(this.transform.position, m_Target.position) < m_MinDistance)
        {
            Attack();
        }
    }

    public override void Attack()
    {
        m_MeleeWeapon.DoAttack();
    }

}
