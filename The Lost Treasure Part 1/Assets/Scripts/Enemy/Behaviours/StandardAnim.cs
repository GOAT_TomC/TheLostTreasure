﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardAnim : AnimBehaviour {

    private Vector3 m_previousPosition;
    private Animator m_Animator;
    private MeleeWeapon m_MeleeWeapon;
    private ObjectPool m_StepSFXPool;

    public override void Initialize(ref Direction dir, Transform currentTransform, MeleeWeapon meleeWeapon, ObjectPool stepObjectPool)
    {
        m_previousPosition = currentTransform.position;
        m_Animator = this.GetComponent<Animator>();
        m_MeleeWeapon = meleeWeapon;
        m_StepSFXPool = stepObjectPool;
    }

    public override void RegisterTotalMovement(ref Direction dir, Transform currentTransform) 
    {
        //Calculate difference in position
        float horizontal = m_previousPosition.x - currentTransform.position.x;
        float vertical = m_previousPosition.y - currentTransform.position.y;

        //Adjust direction to movement
        if (horizontal != 0 || vertical != 0)
        {

            m_Animator.SetBool("IsMoving", true);

            if (horizontal < 0 && Mathf.Abs(horizontal) > Mathf.Abs(vertical))
            {
                dir = Direction.east;
            }
            else if (horizontal > 0 && Mathf.Abs(horizontal) > Mathf.Abs(vertical))
            {
                dir = Direction.west;
            }

            else if (vertical < 0 && Mathf.Abs(horizontal) < Mathf.Abs(vertical))
            {
                dir = Direction.north;
            }
            else if (vertical > 0 && Mathf.Abs(horizontal) < Mathf.Abs(vertical))
            {
                dir = Direction.south;
            }
        }
        else
        {
            m_Animator.SetBool("IsMoving", false);
        }

        m_Animator.SetFloat("Dir", (int)dir);

        if (m_MeleeWeapon != null)
            m_MeleeWeapon.UpdatePosition(dir);

        m_previousPosition = currentTransform.position;
    }

    public void DeactivateTrigger(string name)
    {
        m_Animator.SetBool(name, false);
    }

    public void PlayStepSFX()
    {
        PoolObject SFX = m_StepSFXPool.GetAvailableObjectInPool();
        SFX.transform.position = this.transform.position;
        SFX.Enable();
    }
}
