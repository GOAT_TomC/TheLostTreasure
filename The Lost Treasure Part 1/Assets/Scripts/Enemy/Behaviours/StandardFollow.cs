﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardFollow : FollowBehaviour {

    private const float NEW_TARGET_DIFFERENCE = 0.2f;

    private Transform m_target;
    private Vector3 m_previousPosition;
    private float m_Speed;
    private bool m_allowSearch;
    private Vector3[] m_path;
    private int m_targetIndex;
    [SerializeField] private string m_PathGrid;
    private PathRequestManager PathGridToUse;

    public override Transform Target
    {
        get { return m_target; }
    }

    public override void Instantiate(Transform target, float speed)
    {
        m_target = target;
        m_Speed = speed;
        m_allowSearch = true;
    }

    private void Start()
    {
        PathGridToUse = GameObject.Find(m_PathGrid).GetComponent<PathRequestManager>();
    }

    public override void FindPath()
    {
        if (m_allowSearch && Vector3.Distance(m_previousPosition, m_target.position) >= NEW_TARGET_DIFFERENCE)
        {
            m_targetIndex = 0;
            m_allowSearch = false;
            m_previousPosition = m_target.position;
            PathGridToUse.RequestPath(this.transform.position, m_target.position, OnPathFound);
        }
    }

    public override IEnumerator FollowPath()
    {
        m_targetIndex = 0;
        Vector3 currentWaypoint = m_path[0];

        while (true)
        {
            if (transform.position == currentWaypoint)
            {
                m_targetIndex++;
                if (m_targetIndex >= m_path.Length)
                {
                    yield break;
                }
                currentWaypoint = m_path[m_targetIndex];
            }

            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, m_Speed * Time.deltaTime);
            yield return null;
        }
    }

    public override void OnPathFound(Vector3[] newPath, bool pathSuccesfull)
    {
        m_allowSearch = true;
        if (pathSuccesfull)
        {
            m_path = newPath;
            StopCoroutine("FollowPath");
            StartCoroutine("FollowPath");
        }
    }
}
