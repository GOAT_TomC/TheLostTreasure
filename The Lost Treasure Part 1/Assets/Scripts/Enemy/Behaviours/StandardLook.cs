﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardLook : LookBehaviour {

    private const float MAX_DISTANCE = 10f;

    private Transform m_target;

    [SerializeField] private LayerMask m_Layermask;


    public override Transform Target
    {
        get { return m_target; }
    }

    public override void Instantiate(Transform target)
    {
        m_target = target;
    }

    public override bool Look(Direction direction)
    {
        if (Vector3.Distance(m_target.position - this.transform.position, transform.forward) <= MAX_DISTANCE)
        {
            //float angle = Vector3.Angle(this.transform.position, m_target.position);
            //Vector3 cross = Vector3.Cross(this.transform.position, m_target.position    );
            //if (cross.z < 0) angle = -angle;
            //Debug.Log(angle);
            //if (direction == Direction.north)
            //{
            //    if (angle > 0 && angle < 40)
            //    {
            //        RaycastHit2D rayhit;
            //        rayhit = Physics2D.Raycast(this.transform.position, (m_target.position - this.transform.position), Mathf.Infinity, m_Layermask);
            //        if (rayhit.transform.CompareTag("Player"))
            //        {
            //            //Debug.Log("Player visible");
            //            //return true
            //        }
            //    }
            //}

            //else if (direction == Direction.west)
            //{
            //    if (angle < 50 && angle > 0)
            //    {
            //        RaycastHit2D rayhit;
            //        rayhit = Physics2D.Raycast(this.transform.position, (m_target.position - this.transform.position), Mathf.Infinity, m_Layermask);
            //        if (rayhit.transform.CompareTag("Player"))
            //        {
            //            Debug.Log("Player visible");
            //            //return true
            //        }
            //    }
            //}

            RaycastHit2D rayhit;
            rayhit = Physics2D.Raycast(this.transform.position, (m_target.position - this.transform.position), Mathf.Infinity, m_Layermask);
            if (rayhit.transform.CompareTag("Player"))
            {
                if (!rayhit.transform.GetComponent<PlayerController>().IsStealth)
                {
                    return true;
                }
                else
                    return false;
            }
        }

        return false;
    }
}
