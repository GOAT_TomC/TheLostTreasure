﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dummy : MonoBehaviour {

    [SerializeField] private float health;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GetDamage(float dmg)
    {
        Debug.Log("Damage given: " + dmg);

        health -= dmg;

        if (health < 0)
        {
            this.gameObject.SetActive(false);
        }
    }
}
