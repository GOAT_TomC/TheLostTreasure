﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIUnit : PoolObject {

    private FollowBehaviour m_followBehaviour;
    private LookBehaviour m_lookBehaviour;
    private AnimBehaviour m_animBehaviour;
    private AttackBehaviour m_AttackBehaviour;
    private MeleeWeapon m_MeleeWeapon;
    private DialogueTrigger m_Dialogue;
    [SerializeField] private string m_targetName;
    [SerializeField] private float m_speed;
    [SerializeField] private float health;
    private float m_Health;
    private bool m_AllowAttack = false;

    private ObjectPool m_ObjectPool;

    [SerializeField] private Direction m_aiDirection;

    private bool m_AllowFunctionCall = false;
    private bool m_State = false; //False = non-attack / true = attack

    public override void Initialize(ObjectPool objectPool)
    {
        m_ObjectPool = objectPool;
        m_Health = health;
    }

    // Use this for initialization
    void Awake () {
        m_followBehaviour = this.GetComponent<FollowBehaviour>();
        m_lookBehaviour = this.GetComponent<LookBehaviour>();
        m_animBehaviour = this.GetComponent<AnimBehaviour>();
        m_AttackBehaviour = this.GetComponent<AttackBehaviour>();
        m_Dialogue = this.GetComponent<DialogueTrigger>();
        if (this.transform.childCount > 0)
            m_MeleeWeapon = this.transform.GetChild(0).GetComponent<MeleeWeapon>();

        Transform target = GameObject.Find(m_targetName).transform;

        m_followBehaviour.Instantiate(target, m_speed);
        m_lookBehaviour.Instantiate(target);
        m_animBehaviour.Initialize(ref m_aiDirection, this.transform, m_MeleeWeapon, GameObject.Find("StepSFXPool").GetComponent<ObjectPool>());
        m_AttackBehaviour.Initialize(m_MeleeWeapon, target);
	}

    private void Start()
    {
        GameManager.Instance.DialogueManager.DialogueChange += AllowAttackChange;
    }

    // Update is called once per frame
    void Update ()
    {
        m_animBehaviour.RegisterTotalMovement(ref m_aiDirection, this.transform);

        Decision();
    }

    private void Decision()
    {
        if (m_lookBehaviour.Look(m_aiDirection))
        {
            if (m_Dialogue != null && m_Dialogue.AllowDialogueCall)
                m_Dialogue.TriggerDialogue();

            Debug.Log(m_AllowAttack);
            if (m_AllowAttack)
            {
                if (m_State == false)
                    m_AllowFunctionCall = true;

                m_followBehaviour.FindPath();
                m_AttackBehaviour.CheckForOppertunity();

                if (m_AllowFunctionCall)
                {
                    GameManager.Instance.MusicPlayer.AddAttacking();
                    m_AllowFunctionCall = false;
                }
                m_State = true;
            }
        }
        else
        {
            if (m_State == true)
                m_AllowFunctionCall = true;

            if (m_AllowFunctionCall)
            {
                GameManager.Instance.MusicPlayer.RemoveAttacking();
                m_AllowFunctionCall = false;
            }
            m_State = false;
        }
    }

    public void GetDamage(float dmg)
    {

        health -= dmg;

        if (health < 0)
        {
            if (this.m_ObjectPool != null)
                Disable();
            else
                this.gameObject.SetActive(false);

            GameManager.Instance.MusicPlayer.RemoveAttacking();
        }
    }

    public override void Enable()
    {
        this.gameObject.SetActive(true);
        m_AllowAttack = GameManager.Instance.DialogueManager.CheckForAttackAllowence();
    }

    public override void Disable()
    {
        m_ObjectPool.AddToPool(this);
        this.gameObject.SetActive(false);
        health = m_Health;
    }

    public void AllowAttackChange(bool allowence)
    {
        m_AllowAttack = allowence;
    }
}

public abstract class FollowBehaviour : MonoBehaviour
{
    public abstract Transform Target
    {
        get;
    }

    public abstract IEnumerator FollowPath();

    public abstract void Instantiate(Transform target, float speed);
    public abstract void FindPath();
    public abstract void OnPathFound(Vector3[] newPath, bool pathSuccesfull);
}

public abstract class LookBehaviour : MonoBehaviour
{
    public abstract Transform Target
    {
        get;
    }

    public abstract void Instantiate(Transform target);
    public abstract bool Look(Direction direction);
}

public abstract class AnimBehaviour : MonoBehaviour
{
    public abstract void Initialize(ref Direction dir, Transform currentTransfrom, MeleeWeapon meleeWeapon, ObjectPool stepObjectPool);
    public abstract void RegisterTotalMovement(ref Direction dir, Transform target);
}

public abstract class AttackBehaviour : MonoBehaviour
{
    public abstract void Initialize(MeleeWeapon weapon, Transform target);
    public abstract void CheckForOppertunity();
    public abstract void Attack();
}

