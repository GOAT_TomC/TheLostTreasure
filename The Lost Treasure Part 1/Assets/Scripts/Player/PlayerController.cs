﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    north = 0,
    east,
    south,
    west,
};

public delegate void GetsDamage(float i);
public delegate void GetsHealth(float i);


public class PlayerController : MonoBehaviour {

    private const float REGEN_TIME_FACTOR = 1f;
    private const float REGEN_TIME_ALLOW_FACTOR = 5f;
    private const float HEALTH_REGEN = 10f;

    private Direction dir;

    private Rigidbody2D rb2D;
    private Animator animator;
    private SpriteRenderer srenderer;
    private MeleeWeapon currentWeapon;

    private float health = 100;

    [SerializeField]private float moveSpeed;

    [Header("Interaction")]
    [Tooltip("the layers the player can interact with")]
    [SerializeField]private LayerMask layerMask;

    [Header("Object Pools")]
    [Tooltip("The objectpool from dynamite")]
    [SerializeField]private ObjectPool dynamitePool;
    [SerializeField]private ObjectPool m_StepPool;

    [Header("Stealth")]
    [Tooltip("The transparency of the player in stealth")]
    [SerializeField] private float stealthTransparency;
    [Tooltip("The transparency of the player in not stealth")]
    [SerializeField] private float normalTransparency;

    private bool hasBomb = false;
    private bool isStealth = false;
    private bool allowHealthRegen = true;

    private Coroutine lastAddRoutine;
    private Coroutine lastTimeRoutine;

    public event GetsDamage OnDamage; //We could have used action
    public event GetsHealth OnHealthRegen;
    public event Action OnReset;
    public event Action<bool> InventoryChange; // We could have used a delegate

    public bool IsStealth
    {
        get { return isStealth; }
    }

    // Use this for initialization
    void Start() {
        rb2D = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();
        srenderer = this.GetComponent<SpriteRenderer>();

        if (this.transform.childCount > 0)
        currentWeapon = this.transform.GetChild(0).GetComponent<MeleeWeapon>();
    }

    // Update is called once per frame
    void Update() {
        InputHandler();
        MovementHandler();
        if (health < 100 && allowHealthRegen)
        {
            GetHealth();
        }


    }

    private void MovementHandler()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        if (horizontal != 0 || vertical != 0)
            animator.SetBool("isMoving", true);
        else animator.SetBool("isMoving", false);

        if (Mathf.Abs(vertical) != Mathf.Abs(horizontal))
        {
            if (vertical == 1)
                dir = Direction.north;
            if (vertical == -1)
                dir = Direction.south;
            if (horizontal == 1)
                dir = Direction.east;
            if (horizontal == -1)
                dir = Direction.west;

            rb2D.velocity = (new Vector2(horizontal, vertical) * moveSpeed) * Time.deltaTime;

            if (currentWeapon != null)
            currentWeapon.UpdatePosition(dir);
        }
        else
            rb2D.velocity = Vector2.zero;

        animator.SetFloat("dir", (int)dir);
    }

    private void InputHandler()
    {
        if (Input.GetKeyDown(KeyCode.E)) //Interaction / pick up key
        {
            LookForInteractable();
        }
        if (Input.GetKeyDown(KeyCode.Q) && hasBomb)
        {
            PlaceDynamite();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentWeapon.DoAttack();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            LookForInteractable();
        }
    }

    private void LookForInteractable()//Looks for interactable objects
    {
        RaycastHit2D rayHit;
        Vector3 rayDirection = Vector3.zero;

        if (dir == Direction.north)
            rayDirection = this.transform.up;
        else if (dir == Direction.east)
            rayDirection = this.transform.right;
        else if (dir == Direction.south)
            rayDirection = -this.transform.up;
        else if (dir == Direction.west)
            rayDirection = -this.transform.right;

        rayHit = Physics2D.Raycast(this.transform.position, rayDirection, 0.5f, layerMask);
        if (rayHit)
            InteractWithObject(rayHit);


    }

    private void InteractWithObject(RaycastHit2D rayhitToInteract)//Interacts with objectable object
    {
        switch (rayhitToInteract.transform.tag)
        {
            case "DynamitePickUp":
                if (!hasBomb)
                {
                    hasBomb = true;
                    if (InventoryChange != null)
                        InventoryChange(true);
                }
                break;

            case "Enemy":
                Kick(rayhitToInteract.transform);
                break;

            default:
                Debug.LogError("The rayhit doesn't match any tags");
                break;
        }
    }

    private void PlaceDynamite()
    {
        hasBomb = false;

        if (InventoryChange != null)
            InventoryChange(false);

        PoolObject dynamite = dynamitePool.GetAvailableObjectInPool();
        dynamite.transform.position = this.transform.position;
        dynamite.Enable();
    }

    private void Kick(Transform hitTransform)
    {
        Vector3 force = Vector2.zero;

        if (dir == Direction.north)
            force = this.transform.up;
        else if (dir == Direction.east)
            force = this.transform.right;
        else if (dir == Direction.south)
            force = -this.transform.up;
        else if (dir == Direction.west)
            force = -this.transform.right;

        GameManager.Instance.Physics.AddForceToObject(hitTransform, (force * 0.3f));
    }

    private void Stealth()
    {
        isStealth = !isStealth;

        List<Transform> childs = new List<Transform>();
        if (isStealth)
        {
            
            Color col = srenderer.color;
            col.a = stealthTransparency;
            srenderer.color = col;

            for(int i = 0; i < transform.childCount; ++i)
            {
                childs.Add(this.transform.GetChild(i));
                foreach(Transform child in childs)
                {
                    SpriteRenderer ren = child.GetComponent<SpriteRenderer>();
                    col = ren.color;
                    col.a = stealthTransparency;
                    ren.color = col;
                }
            }
        }
        else
        {
            Color col = srenderer.color;
            col.a = normalTransparency;
            srenderer.color = col;

            for (int i = 0; i < transform.childCount; ++i)
            {
                childs.Add(this.transform.GetChild(i));
                foreach (Transform child in childs)
                {
                    SpriteRenderer ren = child.GetComponent<SpriteRenderer>();
                    col = ren.color;
                    col.a = normalTransparency;
                    ren.color = col;
                }
            }
        }
    }

    public void GetDamage(float dmg)
    {
        allowHealthRegen = false;

        if (lastAddRoutine != null)
            StopCoroutine(lastAddRoutine);
        if (lastTimeRoutine != null)
            StopCoroutine(lastTimeRoutine);

        lastTimeRoutine = StartCoroutine(HealthTimer());

        health -= dmg;
        if (OnDamage != null)
            OnDamage(dmg);

        if (health < 0)
        {
            this.gameObject.SetActive(false);
            this.GetComponent<Respawn>().RespawnPlayer();
        }
    }

    public void GetHealth()
    {
        if (allowHealthRegen)
        {
            allowHealthRegen = false;
            lastAddRoutine = StartCoroutine(AddHealth());
        }

    }

    private IEnumerator AddHealth()
    {
        yield return new WaitForSeconds(REGEN_TIME_FACTOR);
        health += HEALTH_REGEN;
        if (health > 100)
            health = 100;
        if (OnHealthRegen != null)
            OnHealthRegen(HEALTH_REGEN);
        allowHealthRegen = true;
    }

    private IEnumerator HealthTimer()
    {
        yield return new WaitForSecondsRealtime(REGEN_TIME_ALLOW_FACTOR);
        allowHealthRegen = true;
    }

    public void ResetValues()
    {
        health = 100;
        if (OnReset != null)
            OnReset();
    }

    public void ResetVelocity()
    {
        rb2D.velocity = Vector2.zero;
        animator.SetBool("isMoving", false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Stealth"))
        {
            Stealth();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Stealth"))
        {
            Stealth();
        }
    }

    private void PlayStepSFX()
    {
        PoolObject SFX = m_StepPool.GetAvailableObjectInPool();
        SFX.transform.position = this.transform.position;
        SFX.Enable();
    }
}
