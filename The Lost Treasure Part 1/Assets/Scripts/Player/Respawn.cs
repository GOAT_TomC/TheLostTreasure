﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Respawn : MonoBehaviour {

    [SerializeField] private Image m_FadeImg;
    private const float FADE_SPEED = 1f;
    private Color m_Color;

    private Vector3 m_CheckPoint;
    private PlayerController m_Player;

	// Use this for initialization
	void Start () {
        m_Player = this.GetComponent<PlayerController>();
        m_CheckPoint = m_Player.transform.position;

        m_Color = Color.clear;
        m_FadeImg.color = m_Color;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetCheckPoint()
    {
        m_CheckPoint = m_Player.transform.position;
    }

    private void FadeToBlack() // Fade to black to remove sight of player on the game
    {
        m_FadeImg.color = Color.Lerp(m_FadeImg.color, Color.black, FADE_SPEED * Time.deltaTime);
        if (m_FadeImg.color.a <= 0.95f)
        {
            Invoke("FadeToBlack", Time.deltaTime);
        }
        else if (m_FadeImg.color.a > 0.95f)
        {
            ResetValues();
            Invoke("FadeToClear", Time.deltaTime);
        }
    }

    private void FadeToClear()
    {
        m_FadeImg.color = Color.Lerp(m_FadeImg.color, Color.clear, (FADE_SPEED * Time.deltaTime) * 0.5F); // mAGIC NUMBER NASTY
        if (m_FadeImg.color.a > 0.06)
        {
            Invoke("FadeToClear", Time.deltaTime);
        }
        else
        {
            m_FadeImg.gameObject.SetActive(false);
        }
    }

    public void ResetValues()
    {
        m_Player.transform.position = m_CheckPoint;
        m_Player.gameObject.SetActive(true);
        m_Player.ResetValues();
    }

    public void RespawnPlayer()
    {
        m_FadeImg.gameObject.SetActive(true);
        FadeToBlack();
    }




}
