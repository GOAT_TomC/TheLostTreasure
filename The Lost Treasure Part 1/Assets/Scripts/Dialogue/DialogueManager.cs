﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public event Action<bool> DialogueChange;

    [SerializeField] private Text m_NameText;
    [SerializeField] private Text m_DialogueText;

    [SerializeField] private Animator m_Animator;

    private bool m_IsDisplaying = false;
    public bool IsDisplaying { get { return m_IsDisplaying; } }

    private Coroutine m_TypeWriterRoutine;
    private PlayerController m_PlayerController;

    private Queue<string> sentences;

    private Action CallBackDialogueEnd = null;

	// Use this for initialization
	void Start () {
        m_PlayerController = GameObject.FindObjectOfType<PlayerController>();
        sentences = new Queue<string>();
	}

    public void StartDialogue(Dialogue dialogue, Action CallDialogueEnd)
    {

        m_IsDisplaying = true;
        if (DialogueChange != null)
            DialogueChange(false);

        CallBackDialogueEnd = CallDialogueEnd;

        m_PlayerController.ResetVelocity();
        m_PlayerController.enabled = false;

        m_Animator.SetBool("IsOpen", true);
        m_NameText.text = dialogue.m_Name + ":";

        sentences.Clear();
        
        foreach (string sentence in dialogue.m_Sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();

        if (m_TypeWriterRoutine != null)
            StopCoroutine(m_TypeWriterRoutine);

        m_TypeWriterRoutine = StartCoroutine(TypeSentence(sentence));
    }

    private IEnumerator TypeSentence(string stringToType)
    {
        m_DialogueText.text = "";
        foreach (char charracter in stringToType.ToCharArray())
        {
            m_DialogueText.text += charracter;
            yield return null;
        }
    }

    private void EndDialogue()
    {
        m_Animator.SetBool("IsOpen", false);
        m_PlayerController.enabled = true;

        if (DialogueChange != null)
            DialogueChange(true);

        m_IsDisplaying = false;

        if (CallBackDialogueEnd != null)
        {
            CallBackDialogueEnd();
            CallBackDialogueEnd = null;
        }
    }

    public bool CheckForAttackAllowence()
    {
        return (m_IsDisplaying == true ? false : true);
    }
	
    
}
