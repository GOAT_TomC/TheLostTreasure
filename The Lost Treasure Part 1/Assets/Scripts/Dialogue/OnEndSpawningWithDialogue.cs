﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEndSpawningWithDialogue : EndBehaviour {

    private Spawner m_EnemySpawner;

    private bool m_AllowDisplay;

    private void Start()
    {
        m_EnemySpawner = this.GetComponent<Spawner>();
        GameManager.Instance.DialogueManager.DialogueChange += AllowDialogueDisplay;
    }

    public override void OnEnd()
    {
        m_EnemySpawner.ActivateSpawner();

        StartCoroutine(WaitToDisplay());
    }

    private void AllowDialogueDisplay(bool allowence)
    {
        m_AllowDisplay = allowence;
    }

    private IEnumerator WaitToDisplay()
    {
        yield return new WaitForEndOfFrame(); 

        while (m_AllowDisplay == false)
        {
            yield return new WaitForEndOfFrame();
        }

        if (this.GetComponent<DialoguePure>() != null)
            GameManager.Instance.DialogueManager.StartDialogue(this.GetComponent<DialoguePure>().GetDialogue(), null);
    }
}
