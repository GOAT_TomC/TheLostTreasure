﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    public Dialogue m_Dialogue;

    private bool m_AllowDialogueCall = true;
    public bool AllowDialogueCall { get { return m_AllowDialogueCall; } }

    public void TriggerDialogue()
    {
        //FindObjectOfType<DialogueManager>().StartDialogue(m_Dialogue);
        GameManager.Instance.DialogueManager.StartDialogue(m_Dialogue, OnDialogueEnd());
        m_AllowDialogueCall = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (m_AllowDialogueCall)
            {
                TriggerDialogue();
            }
        }
    }

    virtual protected Action OnDialogueEnd()
    {
        return null;
    }
}
