﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTriggerEndEvent : DialogueTrigger {

    [SerializeField] private EndBehaviour m_OnEndBehaviour;

    protected override Action OnDialogueEnd()
    {
        m_OnEndBehaviour.OnEnd();
        return null;
    }
}

public abstract class EndBehaviour : MonoBehaviour
{
    public abstract void OnEnd();
}
