﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePure : MonoBehaviour {

    public Dialogue m_Dialogue;

    public Dialogue GetDialogue()
    {
        return m_Dialogue;
    }
}
