﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Spawner))]
public class FightTrigger : MonoBehaviour {

    private Spawner m_Spawner;
    private bool m_RegisterTrigger = true;
    [SerializeField] private Animator m_Animator;

	// Use this for initialization
	void Start () {
        m_Spawner = this.GetComponent<Spawner>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (m_RegisterTrigger)
        {
            if (collision.CompareTag("Player"))
            {
                m_RegisterTrigger = false;
                m_Animator.SetTrigger("Explode");
                m_Spawner.ActivateSpawner();
            }
        }
    }
}
