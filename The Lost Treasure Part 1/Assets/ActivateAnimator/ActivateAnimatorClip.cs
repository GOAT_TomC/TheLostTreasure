using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class ActivateAnimatorClip : PlayableAsset, ITimelineClipAsset
{
    public ActivateAnimatorBehaviour template = new ActivateAnimatorBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<ActivateAnimatorBehaviour>.Create (graph, template);
        ActivateAnimatorBehaviour clone = playable.GetBehaviour ();
        return playable;
    }
}
