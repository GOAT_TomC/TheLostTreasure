using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using System.Collections.Generic;

[Serializable]
public class ActivateAnimatorBehaviour : PlayableBehaviour
{

    //[SerializeField]
    //private List<GameObject> m_AnimatorsToDisable;

    [SerializeField]
    private List<string> m_Names;

    public override void OnGraphStart (Playable playable)
    {
        for (int i = 0; i < m_Names.Count; ++i)
        {
            GameObject.Find(m_Names[i]).GetComponent<Animator>().enabled = false;
        }
    }
}
